<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('users')->insert([
    		'id' => 4,
            'name' => "Егор Петров",
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'role' => User::ROLE_MANAGER
        ]);

    }
}
