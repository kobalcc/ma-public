<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        	'id' => 1,
            'title' => 'Новая'
        ]);

        DB::table('statuses')->insert([
        	'id' => 2,
            'title' => 'В работе'
        ]);

        DB::table('statuses')->insert([
        	'id' => 3,
            'title' => 'Приостановлена'
        ]);

        DB::table('statuses')->insert([
        	'id' => 4,
            'title' => 'Отменена'
        ]);

        DB::table('statuses')->insert([
        	'id' => 5,
            'title' => 'Исполнена'
        ]);
    }
}
