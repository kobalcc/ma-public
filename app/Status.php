<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{

	const IN_PROGRESS = 2;
	const STATUS_NEW = 1;

    public function orders() {
    	return $this->hasMany('App\Order');
    }

    public function isInProgress() : Bool {
    	return $this->id === self::IN_PROGRESS;
    }

}
