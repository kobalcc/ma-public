<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Order;
use App\Status;

class ManagerController extends Controller {

	public function index() {

		$orders = Order::all();

		// dd($orders);

		return view('manager.index', ['orders' => $orders]);
	}

	public function edit(int $id) {

		$order = Order::findOrFail($id);
		$statuses = Status::all();

		return view('manager.edit', ['order' => $order, 'statuses' => $statuses, 'id' => $id]);

	}

	public function store(int $id, request $data) {

		$order = Order::findOrFail($id);
		$order->status_id = $data['status'];
		$order->price = $data['price'];
		$order->save();

		return redirect('/manager');

	}

}

