<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Status;
use App\Order;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        return view('home', ['data' => $this->active("home")]);
    }

    public function service() {
        return view('service', ['data' => $this->active("service")]);
    }

    public function partner() {
        return view('partner', ['data' => $this->active("partner")]);
    }

    public function privacy() {
        return view('privacy', ['data' => $this->active("")]);
    }

    public function new(request $data) {

        $validation = [
            'name' => 'required',
            'email' => 'required'
        ];

        $this->validate($data, $validation);

        $order = Order::create([
            'status_id' => Status::STATUS_NEW,
            'phone' => $data['phone'],
            'name' => $data['name'],
            'email' => $data['email'],
        ]);

        return json_encode(["status" => "ok"]);
    }

    public function pay(request $data) {
        $order = Order::findOrFail($data['order']);
        if ($data['sign'] == $order->paymentHash()) {
            return view('pay', ['paymentData' => $order->paymentParameters(), 'data' => $this->active("")]);
        } else {
            return json_encode(['status' => 'Invalid sign']);
        }
    }

    private function active($name) {

        $data = [
            "service" => $name == "service" ? "active" : "",
            "home" => $name == "home" ? "active" : "",
            "partner" => $name == "partner" ? "active" : ""
        ];

        return $data;
    }
}
