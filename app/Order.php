<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status_id', 'name', 'email', 'phone', 'price'
    ];

    public function status() {
    	return $this->belongsTo('App\Status');
    }

    public function user() {
    	return $this->belongsTo('App\User');
    }

    public function paymentParameters() {
        $publicKey = env("UNITPAY_PUBLIC_KEY", "");
        return ['publicKey' => $publicKey,
                'sum' => $this->price,
                'account' => $this->id,
                'signature' => $this->getFormSignature(),
                'desc' => "Оплата услуг",
                'locale' => "ru"
        ];
    }

    public function paymentHash() {
        return md5("nndDfeZ22A".$this->id."re33-dd");
    }

    private function getFormSignature() {
        $secretKey = env("UNITPAY_SECRET_KEY", "");
        $hashStr = $this->id.'{up}'."RUB".'{up}'."Оплата услуг".'{up}'.$this->price.'{up}'.$secretKey;
        return hash('sha256', $hashStr);
    }
}
