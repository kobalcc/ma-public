@extends('layouts.main')

@section('content')
    <section class="hero">
      <div class="container">
        <div class="hero__wrap">
          <div class="hero__main">
            <h1 class="hero__title">Эффективный интернет маркетинг</h1>
            <p class="hero__subtitle">Консалтинг, дизайн и продвижение проектов<br><strong> с фокусом на росте вашей прибыли</strong></p>
          </div>
          <div class="hero__cons">
            <div class="hero__cons-inner">
              <h4 class="hero__cons-title">Бесплатная консультация</h4>
              <p class="hero__cons-text">в которой за 15 минут вы увидите, что конкретно вас ждет в работе с нами</p>
              <div class="hero__cons-checkbox">
                <label class="default-checkbox">
                  <input type="checkbox" name="agree" value="agree" hidden checked><span class="default-checkbox__custom"></span>
                </label><a class="hero__agree-link" href="{{ route('home.privacy') }}">Согласие на обработку личных данных</a>
              </div>
            </div>
            <div class="hero__cons-order-text">Заказать консультацию
              <div class="hero__cons-btn">
                <div class="cons-arrow"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="how-work">
      <div class="container">
        <div class="features">
          <div class="features__item">
            <div class="features__img-block">
              <div class="img-wrapper"><img src="images/icons/comp-1.png" alt="feature"></div>
            </div>
            <h5 class="features__title">Контекстная реклама</h5>
            <p class="features__text">Быстрый способ получать клиентов с первого для запуска кампании</p>
          </div>
          <div class="features__item">
            <div class="features__img-block">
              <div class="img-wrapper"><img src="images/icons/comp-2.png" alt="feature"></div>
            </div>
            <h5 class="features__title">SEO продвижение</h5>
            <p class="features__text">Инвестиции в устойчивое будущее вашего бизнеса</p>
          </div>
          <div class="features__item">
            <div class="features__img-block">
              <div class="img-wrapper"><img src="images/icons/comp-3.png" alt="feature"></div>
            </div>
            <h5 class="features__title">Маркетинг в соц. сетях</h5>
            <p class="features__text">Новые клиенты, повышение лояльности к бренду, и улучшение репутации</p>
          </div>
          <div class="features__item">
            <div class="features__img-block">
              <div class="img-wrapper"><img src="images/icons/comp-4.png" alt="feature"></div>
            </div>
            <h5 class="features__title">Создание сайтов</h5>
            <p class="features__text">Ваш сайт — это отражение вашего бизнеса</p>
          </div>
        </div>
        <div class="how-work__wrap">
          <h2 class="how-work__title">Как мы работаем</h2>
          <div class="how-work__item">
            <div class="how-work__text-block">
              <div class="how-work__item-header">
                <h3 class="how-work__item-title">Настраиваем подробную веб аналитику<span>01</span></h3>
                <p class="how-work__item-text">В основе нашей работы мы заложили подход маркетинга, основанного на данных. Поэтому в начале мы всегда настраиваем и подключаем всю необходимую аналитику, даже у самых сложных проектов, с любыми специфическими требованиями. Анализируем конъюнктуру, понимаем цели и задачи в разных отраслях, строим стратегии, подбираем и используем эффективные инструменты маркетинга.</p>
              </div>
            </div>
            <div class="how-work__img-block">
              <div class="img-wrapper"><img src="images/icons/loupe.svg" alt="work"></div>
            </div>
          </div>
          <div class="how-work__item how-work__item--reverse">
            <div class="how-work__text-block">
              <div class="how-work__item-header">
                <h3 class="how-work__item-title">Рекламные инструменты Яндекс и Google<span>02</span></h3>
                <p class="how-work__item-text">Это позволит вашему бизнесу получить клиентов уже в первую неделю работы и окупить рекламный бюджет. Параллельно мы собираем аналитические данные и разрабатываем индивидуальную маркетинговую стратегию на 3 и 6 месяцев. Далее мы подключаем ряд эффективных инструментов маркетинга Google+, таргетинг, SEO, развитие через форумы и тематические порталы, e-mail маркетинг - все эти рекламные каналы тестируем в условиях вашего бизнеса и подключаем только эффективные.</p>
              </div>
            </div>
            <div class="how-work__img-block">
              <div class="img-wrapper"><img src="images/icons/people-126x126.svg" alt="work"></div>
            </div>
          </div>
          <div class="how-work__item">
            <div class="how-work__text-block">
              <div class="how-work__item-header">
                <h3 class="how-work__item-title">Оптимизация и ежедневная работа<span>03</span></h3>
                <p class="how-work__item-text">Высокие показатели конверсии возможны только благодаря ежедневной работе над улучшением, каждого рекламного канала, сайта и вашего предложения. Проведение А/В тестов и сбор всех необходимых данных позволяет полностью раскрыть потенциал сайта и рекламных каналов.</p>
              </div>
            </div>
            <div class="how-work__img-block">
              <div class="img-wrapper"><img src="images/icons/gears-124x126.svg" alt="work"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="main-cons">
      <div class="container">
        <blockquote class="main-cons__blockquote">Основная метрика в нашем бизнесе, это LTV (life time value). Мы с максимальной отдачей выполняем свою работу, следим за качеством и не срываем сроков, работа над той частью бизнеса клиента, которую он поручил нам, ведется практически 24/7. Главный принцип нашей кампании, давать клиентам больше чем они ожидают
          <div class="main-cons__blockquote-author">CEO</div>
          <div class="quote left">&#8220;</div>
          <div class="quote right">&#8220;</div>
        </blockquote>
        <form class="main-form form-consult" name="form-consult">
            @csrf
          <div class="main-form__group">
            <div class="form-group">
              <input type="text" placeholder="Ваше имя" name="name" data-msg-required="Это поле необходимо заполнить.">
            </div>
            <div class="form-group">
              <input type="email" placeholder="Ваш email" name="email" data-msg-required="Это поле необходимо заполнить." data-msg-email="Пожалуйста, введите корректный адрес электронной почты.">
            </div>
            <button class="btn main-form__submit" type="submit">Заказать звонок<span></span></button>
          </div>
        </form>
      </div>
    </section>
    <section class="geo">
      <div class="container">
        <h2 class="geo__title">география клиентов</h2>
        <p class="geo__subtitle">Команда Team Overflow помогает строить бизнес, клиентам по всему миру. Станьте одним из них!</p>
        <div class="geo__map-block">
          <div class="img-wrapper"><img src="images/map.svg" alt="map">
            <div class="geo__point-wrap geo__point-wrap-1">
              <div class="geo__point geo__point-1"></div>
              <div class="geo__dropdown">
                <div class="geo__dropdown-header">
                  <div class="img-wrapper"><img src="images/companies/cyprus.svg" alt="company"></div>
                  <div class="geo__dropdown-title">cyprus developers<span> alliance</span></div>
                </div><a class="geo__dropdown-link" href="#" target="_blank">https://cyprus-alliance.com/</a>
                <p class="geo__dropdown-text">Cyprus Developers Alliance Ltd.Лимасол, Кипр</p>
              </div>
            </div>
            <div class="geo__point-wrap geo__point-wrap-2">
              <div class="geo__point geo__point-2"></div>
              <div class="geo__dropdown">
                <div class="geo__dropdown-header">
                  <div class="img-wrapper"><img src="images/companies/cyprus.svg" alt="company"></div>
                  <div class="geo__dropdown-title">cyprus developers<span> alliance</span></div>
                </div><a class="geo__dropdown-link" href="#" target="_blank">https://cyprus-alliance.com/</a>
                <p class="geo__dropdown-text">Cyprus Developers Alliance Ltd.Лимасол, Кипр</p>
              </div>
            </div>
            <div class="geo__point-wrap geo__point-wrap-3">
              <div class="geo__point geo__point-3"></div>
              <div class="geo__dropdown">
                <div class="geo__dropdown-header">
                  <div class="img-wrapper"><img src="images/companies/cyprus.svg" alt="company"></div>
                  <div class="geo__dropdown-title">cyprus developers<span> alliance</span></div>
                </div><a class="geo__dropdown-link" href="#" target="_blank">https://cyprus-alliance.com/</a>
                <p class="geo__dropdown-text">Cyprus Developers Alliance Ltd.Лимасол, Кипр</p>
              </div>
            </div>
            <div class="geo__point-wrap geo__point-wrap-4">
              <div class="geo__point geo__point-4"></div>
              <div class="geo__dropdown">
                <div class="geo__dropdown-header">
                  <div class="img-wrapper"><img src="images/companies/cyprus.svg" alt="company"></div>
                  <div class="geo__dropdown-title">cyprus developers<span> alliance</span></div>
                </div><a class="geo__dropdown-link" href="#" target="_blank">https://cyprus-alliance.com/</a>
                <p class="geo__dropdown-text">Cyprus Developers Alliance Ltd.Лимасол, Кипр</p>
              </div>
            </div>
            <div class="geo__point-wrap geo__point-wrap-5">
              <div class="geo__point geo__point-5"></div>
              <div class="geo__dropdown">
                <div class="geo__dropdown-header">
                  <div class="img-wrapper"><img src="images/companies/cyprus.svg" alt="company"></div>
                  <div class="geo__dropdown-title">cyprus developers<span> alliance</span></div>
                </div><a class="geo__dropdown-link" href="#" target="_blank">https://cyprus-alliance.com/</a>
                <p class="geo__dropdown-text">Cyprus Developers Alliance Ltd.Лимасол, Кипр</p>
              </div>
            </div>
            <div class="geo__point-wrap geo__point-wrap-6">
              <div class="geo__point geo__point-6"></div>
              <div class="geo__dropdown">
                <div class="geo__dropdown-header">
                  <div class="img-wrapper"><img src="images/companies/cyprus.svg" alt="company"></div>
                  <div class="geo__dropdown-title">cyprus developers<span> alliance</span></div>
                </div><a class="geo__dropdown-link" href="#" target="_blank">https://cyprus-alliance.com/</a>
                <p class="geo__dropdown-text">Cyprus Developers Alliance Ltd.Лимасол, Кипр</p>
              </div>
            </div>
            <div class="geo__point-wrap geo__point-wrap-7">
              <div class="geo__point geo__point-7"></div>
              <div class="geo__dropdown">
                <div class="geo__dropdown-header">
                  <div class="img-wrapper"><img src="images/companies/cyprus.svg" alt="company"></div>
                  <div class="geo__dropdown-title">cyprus developers<span> alliance</span></div>
                </div><a class="geo__dropdown-link" href="#" target="_blank">https://cyprus-alliance.com/</a>
                <p class="geo__dropdown-text">Cyprus Developers Alliance Ltd.Лимасол, Кипр</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <section class="main-audit">
      <div class="container">
        <h2 class="main-audit__title">Закажите бесплатный аудит вашего маркетинга</h2>
        <form class="main-form form-audit" name="form-audit">
            @csrf
          <div class="main-form__group">
            <div class="form-group">
              <input type="text" placeholder="Ваше имя" name="name" data-msg-required="Это поле необходимо заполнить.">
            </div>
            <div class="form-group">
              <input type="email" placeholder="Ваш email" name="email" data-msg-required="Это поле необходимо заполнить." data-msg-email="Пожалуйста, введите корректный адрес электронной почты.">
            </div>
            <button class="btn main-form__submit" type="submit">Заказать аудит<span></span></button>
          </div>
        </form>
      </div>
    </section>
@endsection