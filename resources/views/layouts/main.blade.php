<?php extract($data); ?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="date=no">
    <meta name="format-detection" content="address=no">
    <meta name="format-detection" content="email=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="notranslate" name="google">
    <!--Start of Social Graph Protocol Meta Data--><meta property="og:locale" content="ru-Ru">
<meta property="og:type" content="website">
<meta property="og:title" content="Team Overflow">
<meta property="og:description" content="Эффективный интернет маркетинг">
    <!--End of Social Graph Protocol Meta Data-->
    <link rel="apple-touch-icon" href="/images/favicon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.png">
    <link rel="icon" type="image/png" href="/images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <!--link(rel='manifest', href='/site.webmanifest')-->
    <!--link(rel='mask-icon', href='/img/favicon/safari-pinned-tab.svg', color='#5bbad5')-->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <title>Team Overflow</title>
    <link rel="stylesheet" href="css/libs.min.css">
    <link rel="stylesheet" href="css/style.min.css?v8">
    <script src="https://widget.unitpay.ru/unitpay.js"></script>
  </head>
  <body>
    <header class="header">
      <div class="container">
        <div class="header__wrap">
          <div class="header__support">
            <div class="img-wrapper header__support-img-wrap"><img src="images/support.png" alt="support"></div>
          </div><a class="header__logo img-wrapper" href="{{ route('home') }}"><img src="images/logo.svg" alt="logo"></a>
          <div class="nav-wrap">
            <div class="nav-wrap__inner">
              <nav class="nav">
                <ul class="nav__list">
                  <li class="nav__item"><a class="{{ $home }} nav__link" href="{{ route('home') }}">Главная</a></li>
                  <li class="nav__item"><a class="{{ $service }} nav__link" href="{{ route('home.service') }}">Услуги</a></li>
                  <li class="nav__item"><a class="{{ $partner }} nav__link" href="{{ route('home.partner') }}">Партнерам</a></li>
                </ul>
              </nav>
            </div>
          </div>
          <div class="header__contacts-wrap">
            <div class="header__contacts-panel">
              <div class="header__contacts-inner">
                <div class="header__contacts-close"></div><a class="header__tel" href="tel:+7 967 333 33 11">+7 967 333 33 11</a>
                
                <div class="header__contacts-bottom">
                  <p>ООО «ОнЛинк»</p>
                </div>
              </div>
            </div>
            
            <div class="hamburger hamburger--spring js-hamburger">
              <div class="hamburger-box">
                <div class="hamburger-inner"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    
	@yield('content')
    
    <footer class="footer">
      <div class="container">
        <div class="footer__main">
          <div class="footer__nav">
            <div class="footer__nav-title">Навигация:</div>
            <ul class="footer__nav-list">
              <li class="footer__nav-item"><a class="{{ $home }} nav__link" href="{{ route('home') }}">Главная</a></li>
              <!--    a(href="index.html").footer__nav-link Главная-->
              <!--li.footer__nav-item-->
              <!--    a(href="service.html").footer__nav-link Услуги-->
              <!--li.footer__nav-item-->
              <!--    a(href="partner.html").footer__nav-link Партнёрам-->
              <li class="footer__nav-item"><a class="{{ $service }} nav__link" href="{{ route('home.service') }}">Услуги</a></li>
              <!--    a(href="index.html").footer__nav-link Главная-->
              <!--li.footer__nav-item-->
              <!--    a(href="service.html").footer__nav-link Услуги-->
              <!--li.footer__nav-item-->
              <!--    a(href="partner.html").footer__nav-link Партнёрам-->
              <li class="footer__nav-item"><a class="{{ $partner }} nav__link" href="{{ route('home.partner') }}">Партнерам</a></li>
              <!--    a(href="index.html").footer__nav-link Главная-->
              <!--li.footer__nav-item-->
              <!--    a(href="service.html").footer__nav-link Услуги-->
              <!--li.footer__nav-item-->
              <!--    a(href="partner.html").footer__nav-link Партнёрам-->
            </ul>
          </div>
          <div class="footer__slider-block">
            <h3 class="footer__slider-title">Сертифицированное агентство</h3>
          </div>
          <div class="footer__requisit">
            <ul class="footer__req-list">
              <li class="footer__req-item">ООО «ОнЛинк»</li>
            </ul><a class="btn-trans" href="/pdf/price.pdf" target="_blank">Прайслист<span></span></a>
          </div>
        </div>
      </div>
      <div class="footer__bottom">
        <div class="container">
          <div class="footer__bottom-wrap">
            <div class="footer__rights">2020 © All Rights Reserved</div><a class="footer__policy" href="{{ route('home.privacy') }}">Privacy Policy</a>
          </div>
        </div>
      </div>
    </footer>
    <script src="js/libs.min.js"></script>
    <script src="js/scripts.min.js?v2"></script>
    <script src="js/rs.js"></script>
    <script src="js/pages-switcher.js"></script>
    <div class="default-modal modal-consult">
      <div class="default-modal__content">
        <div class="default-modal__close"></div>
        <h2 class="default-modal__title"> бесплатная консультация</h2>
        <form class="form-modal" name="form-modal-consult">
        	@csrf
          <div class="form-group">
            <input type="text" name="name" placeholder="Ваше имя" data-msg-required="Это поле необходимо заполнить.">
          </div>
          <div class="form-group">
            <input type="email" name="email" placeholder="Ваш Email" data-msg-required="Это поле необходимо заполнить." data-msg-email="Пожалуйста, введите корректный адрес электронной почты.">
          </div>
          <div class="form-group">
            <input type="tel" name="phone" placeholder="Ваш телефон" data-msg-required="Это поле необходимо заполнить." data-msg-checkMin="Введите не меньше 11 символов">
          </div>
          <div class="default-modal__footer">
            <button class="btn modal-consult__submit" type="submit">Заказать консультацию<span></span></button>
          </div>
        </form>
      </div>
    </div>
    <div class="default-modal modal-thx">
      <div class="default-modal__content">
        <div class="default-modal__close"></div>
        <h3 class="default-modal__title"> Спасибо за заявку</h3>
        <p class="modal-thx__text">Наши менеджеры свяжутся с Вами в ближайшее время.</p>
      </div>
    </div>
    <div class="default-modal modal-review-main modal-review-1">
      <div class="default-modal__content">
        <div class="default-modal__close"></div>
        <div class="default-modal__header">
          <div class="img-wrapper"><img src="images/companies/cyprus-all.png" alt="company"></div>
          <div class="modal-review-main__contacts">
            <p>Phone: +357 2531 7998</p>
            <p>Email: passport@cypruslaw.org</p>
            <p>ALFA TOWER, office 31, 4 Pavlou Nirvana,</p>
            <p>3021 Limassol, Cyprus</p>
          </div>
        </div>
        <h2 class="default-modal__title">отзыв о сотрудничестве</h2>
        <p class="modal-review-main__text">В основе нашей работы мы заложили подход маркетинга, основанного на данных. Поэтому в начале мы всегда настраиваем и подключаем всю необходимую аналитику, даже у самых сложных проектов, с любыми специфическими требованиями. Анализируем конъюнктуру, понимаем цели и задачи в разных отраслях, строим стратегии, подбираем и используем эффективные инструменты маркетинга.</p>
        <div class="default-modal__footer">
          <p class="modal-review-main__who">Commercial Director, Kirill Pretkel</p>
        </div>
      </div>
    </div>
  </body>
</html>