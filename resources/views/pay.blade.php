@extends('layouts.main')
<?php extract($paymentData); ?>
@section('content')
<section class="main-cons">
      <div class="container">
        <script type="text/javascript">
            document.addEventListener('DOMContentLoaded', function() {
              return pay();
            });

            this.pay = function() {
                var payment = new UnitPay();
                payment.createWidget({
                    publicKey: "{{ $publicKey }}",
                    sum: "{{ $sum }}",
                    account: "{{ $account }}",
                    signature: "{{ $signature }}",
                    desc: "{{ $desc }}",
                    locale: "{{ $locale }}",
                });
                payment.success(function (params) {
                    console.log('Успешный платеж');
                });
                payment.error(function (message, params) {
                    console.log(message);
                });
                return false;
            };
        </script>
      </div>
    </section>
@endsection
