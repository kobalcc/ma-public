<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta name="format-detection" content="date=no">
    <meta name="format-detection" content="address=no">
    <meta name="format-detection" content="email=no">
    <meta content="notranslate" name="google">
    <!--Start of Social Graph Protocol Meta Data--><meta property="og:locale" content="ru-Ru">
<meta property="og:type" content="website">
<meta property="og:title" content="{{ config('app.name', 'AUTOSERVICE-3000') }}">
<meta property="og:description" content="Эффективный интернет маркетинг">
    <!--End of Social Graph Protocol Meta Data-->
    <link rel="apple-touch-icon" href="/images/favicon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon.png">
    <link rel="icon" type="image/png" href="/images/favicon.png">
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <!--link(rel='manifest', href='/site.webmanifest')-->
    <!--link(rel='mask-icon', href='/img/favicon/safari-pinned-tab.svg', color='#5bbad5')-->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <title>{{ config('app.name', 'AUTOSERVICE-3000') }}</title>
    <link rel="stylesheet" href="css/libs.min.css">
    <link rel="stylesheet" href="css/style.min.css?v8">
  </head>
  <body>
    <section class="not-f">
      <div class="container">
        <div class="img-wrapper"><img src="images/404.png" alt="owl"></div>
        <h1 class="visually-hidden">Team Overflow</h1>
        <h2 class="not-f__title">Error 404</h2>
        <p class="not-f__text">The page were looking was moved or doesn’t exist.</p><a class="btn" href="/">на главную<span></span></a>
      </div>
    </section>
    <script src="js/libs.min.js"></script>
    <script src="js/scripts.min.js"></script>
    <script src="js/rs.js"></script>
    <script src="js/pages-switcher.js"></script>
  </body>
</html>