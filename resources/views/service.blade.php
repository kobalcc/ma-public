@extends('layouts.main')

@section('content')
    <section class="s-hero">
      <div class="container">
        <h1 class="s-hero__title">Профессиональная настройка и ведение<br> контекстной рекламы Google AdWords и Яндекс Директ</h1>
        <p class="s-hero__text">Контекстная реклама в Яндекс и Google даёт возможность получить заказы с первого дня работы. Ваши текстовые объявления будут отображаться на первой странице поиска по ряду ключевых запросов, а графические баннера появятся на сайтах-партнерах Яндекс и Google.</p>
        <form class="main-form form-service-consult" name="form-consult">
          @csrf
          <div class="main-form__group">
            <div class="form-group">
              <input type="text" placeholder="Ваше имя" name="name" data-msg-required="Это поле необходимо заполнить.">
            </div>
            <div class="form-group">
              <input type="email" placeholder="Ваш email" name="email" data-msg-required="Это поле необходимо заполнить." data-msg-email="Пожалуйста, введите корректный адрес электронной почты.">
            </div>
            <button class="btn main-form__submit" type="submit">Заказать звонок<span></span></button>
          </div>
          <div class="form-checkbox">
            <label class="default-checkbox">
              <input type="checkbox" name="agree" hidden data-msg-required="Это поле необходимо заполнить." checked><span class="default-checkbox__custom"></span>
            </label><a class="hero__agree-link" href="confid.html">Согласие на обработку личных данных</a>
          </div>
        </form>
      </div>
    </section>
    <section class="s-features create-ef">
      <div class="container">
        <h2 class="s-features__title">Создание эффективных компаний</h2>
        <p class="s-features__subtitle">Создание рекламных компаний, которые будут приносить результаты, требует большого количества исследований и анализа. Благодаря стратегическому и тактическому подходу в контекстной рекламе, результат любого клиента может стать управляемым.</p>
        <ul class="s-features__grid">
          <li class="s-features__item">
            <div class="s-features__header">
              <div class="img-wrapper"><img src="images/icons/feature-1.svg" alt="icon"></div>
            </div>
            <p class="s-features__text">Изучаем бизнес клиента.<br> 
Мы не просто настраиваем контекст, а детально анализируем слабые и сильные стороны клиента. Наши рекомендации по продукту помогают достичь максимального эффекта от рекламы.</p>
          </li>
          <li class="s-features__item">
            <div class="s-features__header">
              <div class="img-wrapper"><img src="images/icons/feature-2.svg" alt="icon"></div>
            </div>
            <p class="s-features__text">Начинаем работу с качественной настройки web-аналитики. Настраиваем точную аналитику для самых сложных проектов. Это помогает получать твёрдую и своевременную информацию, качественно анализировать аудиторию и принимать правильные решения.</p>
          </li>
          <li class="s-features__item">
            <div class="s-features__header">
              <div class="img-wrapper"><img src="images/icons/feature-3.svg" alt="icon"></div>
            </div>
            <p class="s-features__text">Точный таргетинг.  Мы тестируем различные ключевые слова, генерируем гипотезы и ищем вашу ЦА в различных направлениях. Контекстная реклама для нас  безграничное творчество. Такой подход позволяет достигать и поддерживать высокие результаты.</p>
          </li>
        </ul>
      </div>
    </section>
    <section class="optim">
      <div class="container">
        <div class="optim__grid">
          <div class="optim__main">
            <h2 class="optim__title">Постоянная оптимизация компаний</h2>
            <p class="optim__subtitle">Мы считаем, что дальнейшая поддержка и работа над PPC компаниями также важны, как и грамотная начальная настройка.</p>
            <ul class="optim__list">
              <li class="optim__item">Опыт работы со сложными аккаунтами и крупными бюджетами. Мы ведем кампании с разветвленной структурой и охватом по всему миру. Ежедневная работа над аккаунтами с совокупным рекламным бюджетом более 4 000 000 рублей в месяц, позволяет нам находить новые решения, исключать ошибки и достигать высоких результатов.</li>
              <li class="optim__item">Отслеживаем тренды и тенденции. Тенденции пользователей могут помочь нам определить, как может потребоваться изменение объявления, чтобы быть успешным. Мы отслеживаем эти тенденции, чтобы увидеть, где происходят клики, и как ваше объявление может быть пересмотрено, чтобы воспользоваться преимуществами.</li>
              <li class="optim__item">Специалисты на связи 24/7 Вносим правки в любое время. Внимательно следим за ситуацией на рынке и активностью конкурентов. Ежедневно работаем над поиском новых точек роста для вашего бизнеса.</li>
            </ul>
          </div>
          <div class="img-wrapper"><img src="images/optim.png" alt="diagram"></div>
        </div>
      </div>
    </section>
    <section class="s-features reporting">
      <div class="container">
        <h2 class="s-features__title">Отчетность о результатах работы</h2>
        <p class="s-features__subtitle">Мы гарантируем, что вам больше не придется строить предположения об эффективности ваших рекламных компаний. Любые изменения в показателях, новые тенденции и результаты наших тестов найдут отражение в регулярной отчетности.</p>
        <ul class="s-features__grid">
          <li class="s-features__item">
            <div class="s-features__header">
              <div class="img-wrapper"><img src="images/icons/feature-4.svg" alt="icon"></div>
            </div>
            <h5 class="s-features__item-title">Автоматизация и контроль
качества</h5>
            <p class="s-features__text">Автоматизация и контроль качества. Мы активно сочетаем использование ручных методов и автоматизированных продуктов.</p>
          </li>
          <li class="s-features__item">
            <div class="s-features__header">
              <div class="img-wrapper"><img src="images/icons/feature-5.svg" alt="icon"></div>
            </div>
            <h5 class="s-features__item-title">Данные о конверсиях</h5>
            <p class="s-features__text">Данные о конверсиях.<br> 
Построим четкую взаимосвязь между ключевыми словами, объявлениями и конверсиями для вашего бизнеса.</p>
          </li>
          <li class="s-features__item">
            <div class="s-features__header">
              <div class="img-wrapper"><img src="images/icons/feature-6.svg" alt="icon"></div>
            </div>
            <h5 class="s-features__item-title">Отчеты всегда у вас<br>
под рукой</h5>
            <p class="s-features__text">Отчеты всегда у вас под рукой.
Мы предоставим любую отчетность, в удобном формате. Все необходимые бизнес показатели в онлайн режиме</p>
          </li>
        </ul>
      </div>
    </section>
    <section class="s-audit">
      <div class="container">
        <h2 class="s-audit__title">Закажите бесплатный аудит вашего маркетинга</h2>
        <ul class="s-audit__list">
          <li class="s-audit__item">Анализ и рекомендации по текущим кампаниям</li>
          <li class="s-audit__item">Индивидуальная консультация нашего специалиста</li>
          <li class="s-audit__item">Прогноз бюджета и объемов трафика</li>
          <li class="s-audit__item">Развёрнутый план дальнейших действий</li>
        </ul>
        <form class="main-form form-audit" name="form-audit">
          @csrf
          <div class="main-form__group">
            <div class="form-group">
              <input type="text" placeholder="Ваше имя" name="name" data-msg-required="Это поле необходимо заполнить.">
            </div>
            <div class="form-group">
              <input type="email" placeholder="Ваш email" name="email" data-msg-required="Это поле необходимо заполнить." data-msg-email="Пожалуйста, введите корректный адрес электронной почты.">
            </div>
            <button class="btn main-form__submit" type="submit">Заказать аудит<span></span></button>
          </div>
        </form>
      </div>
    </section>
@endsection