@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">Заявка: {{ $order->name . ", " . $order->email }}</div>

                <div class="card-body">
                    
                    <form method="POST" action="{{ route('manager.store', ['id' => $order->id]) }}">
                        @csrf

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Цена</label>

                            <div class="col-md-6">
                                <input id="price" type="text" class="form-control" name="price" value="{{ $order->price }}" autocomplete="none" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Статус</label>

                            <div class="col-md-6">

                              <select id="status" class="form-control", name="status">
                                @foreach($statuses as $status)
                                  @if ($status->id == $order->status->id)
                                    <option value="{{ $status->id }}" selected>{{ $status->title }}</option>
                                  @else
                                    <option value="{{ $status->id }}">{{ $status->title }}</option>
                                  @endif
                                @endforeach
                              </select>

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Сохранить
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
