@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">Клиент {{ $client->name }}</div>

                <div class="card-body">
                    
                    <p>
                      <b>Имя:</b> {{ $client->name }}</br>
                      <b>Email:</b> {{ $client->email }}
                    </p>

                    <h1>История заявок</h1>

                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Статус</th>
                          <th scope="col">Телефон</th>
                          <th scope="col">VIN</th>
                          <th scope="col">Информация</th>
                          <th scope="col">Дополнительно</th>
                        </tr>
                      </thead>
                      <tbody>

                        @foreach($client->orders as $order)

                            <tr>
                              <th scope="row">{{ $order->id }}</th>
                              <td>{{ $order->status->title }}</td>
                              <td>{{ $order->phone }}</td>
                              <td>{{ $order->vin }}</td>
                              <td>{{ $order->auto }}</td>
                              <td>{{ $order->status->isInProgress() ? "Осталось " . random_int(1, 10) . " ч." : "" }}</td>
                            </tr>

                        @endforeach

                      </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection