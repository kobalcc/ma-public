@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">Клиенты</div>

                <div class="card-body">

                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Имя</th>
                          <th scope="col">Email</th>
                        </tr>
                      </thead>
                      <tbody>

                        @foreach($clients as $client)
                        	
                            <tr>
                              <th scope="row">{{ $client->id }}</th>
                              <td><a href="{{ route('manager.client', ['id' => $client->id]) }}"> {{ $client->name }} </a> </td>
                              <td>{{ $client->email }}</td>
                            </tr>

                        @endforeach

                      </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection