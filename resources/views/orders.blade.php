@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="card">
                <div class="card-header">Мои заявки</div>

                <div class="card-body">
                    
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Статус</th>
                          <th scope="col">Имя</th>
                          <th scope="col">Email</th>
                          <th scope="col">Телефон</th>
                          <th scope="col">Цена</th>
                        </tr>
                      </thead>
                      <tbody>

                        @foreach($orders as $order)

                            <tr>
                              <th scope="row">{{ $order->id }}</th>
                              <td>{{ $order->status->title }}</td>
                              <td>{{ $order->name }}</td>
                              <td>{{ $order->email }}</td>
                              <td>{{ $order->phone }}</td>
                              <td>{{ $order->price }}</td>
                            </tr>

                        @endforeach

                      </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection