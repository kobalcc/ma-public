@extends('layouts.main')

@section('content')
    <section class="p-hero">
      <div class="container">
        <h1 class="p-hero__title">Партнёрская программа</h1>
        <p class="p-hero__text">Стройте свой бизнес вместе с нами и зарабатывайте от<span> 10</span> до<span> 25%</span> стоимости наших услуг!
          <form class="main-form form-partner-consult" name="form-consult">
            @csrf
            <div class="main-form__group">
              <div class="form-group">
                <input type="text" placeholder="Ваше имя" name="name" data-msg-required="Это поле необходимо заполнить.">
              </div>
              <div class="form-group">
                <input type="email" placeholder="Ваш email" name="email" data-msg-required="Это поле необходимо заполнить." data-msg-email="Пожалуйста, введите корректный адрес электронной почты.">
              </div>
              <button class="btn main-form__submit" type="submit">Заказать звонок<span></span></button>
            </div>
            <div class="form-checkbox">
              <label class="default-checkbox">
                <input type="checkbox" name="agree" hidden data-msg-required="Это поле необходимо заполнить." checked><span class="default-checkbox__custom"></span>
              </label><a class="hero__agree-link" href="confid.html">Согласие на обработку личных данных</a>
            </div>
          </form>
        </p>
      </div>
    </section>
    <section class="how-part">
      <div class="container">
        <h2 class="how-part__title">как стать нашим партнером</h2>
        <div class="how-part__grid">
          <div class="how-part__item"><img class="how-part__img-bg" src="images/how-part-1.svg" alt="img">
            <div class="img-wrapper"><img src="images/icons/phone.svg" alt="img"></div>
            <p class="how-part__text">присоединяйтесь к партнерской программе</p>
          </div>
          <div class="how-part__item"><img class="how-part__img-bg" src="images/how-part-2.svg" alt="img">
            <div class="img-wrapper"><img src="images/icons/hands.svg" alt="img"></div>
            <p class="how-part__text">Свяжитесь с нами и получите свой реферальный код</p>
          </div>
          <div class="how-part__item"><img class="how-part__img-bg" src="images/how-part-2.svg" alt="img">
            <div class="img-wrapper"><img src="images/icons/rupor.svg" alt="img"></div>
            <p class="how-part__text">Рекомендуйте нас своим клиентам и подписчикам</p>
          </div>
          <div class="how-part__item"><img class="how-part__img-bg" src="images/how-part-3.svg" alt="img">
            <div class="img-wrapper"><img src="images/icons/pig.svg" alt="img"></div>
            <p class="how-part__text">зарабатывайте от 10 до 25% от сделок</p>
          </div>
        </div>
        <ul class="how-part__list">
          <li class="how-part__list-item">Независимо от количества оплат и без ограничения по времени! Партнерская программа от overflow.team — это выгодное накопительное долгосрочное сотрудничество с экспертным сервисом в сфере продвижения сайтов и услуг в сети, с высокой конверсией трафика и высокими темпами роста постоянного дохода.</li>
          <li class="how-part__list-item">Привлекая одного клиента, вы гарантированно получаете процент от заключенной сделки. Данное условие действует только на новых клиентов. За привлечение действующих клиентов комиссия не засчитывается. Ваш клиент получает 5% скидку на все услуги!</li>
        </ul>
      </div>
    </section>
@endsection