<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/service', 'HomeController@service')->name('home.service');
Route::get('/partner', 'HomeController@partner')->name('home.partner');
Route::get('/privacy', 'HomeController@privacy')->name('home.privacy');
Route::get('/orders', 'HomeController@orders')->name('orders');
Route::post('/orders/store', 'HomeController@new')->name('orders.new');
Route::get('/pay', 'HomeController@pay')->name('pay');

Route::group(

	[
		'prefix' => 'manager',
		'middleware' => [
			'auth',
			'can:manager'
		]
	],
	function() {

		Route::get('/', 'ManagerController@index')->name('manager.index');
		Route::get('/edit/{id}', 'ManagerController@edit')->name('manager.edit');
		Route::post('/store/{id}', 'ManagerController@store')->name('manager.store');
	}

);