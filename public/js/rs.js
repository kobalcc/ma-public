$(function() {

    function getScrollbarWidth() {
        var outer = document.createElement("div");
        outer.style.visibility = "hidden";
        outer.style.width = "100px";
        outer.style.msOverflowStyle = "scrollbar";

        document.body.appendChild(outer);

        var widthNoScroll = outer.offsetWidth;
        // force scrollbars
        outer.style.overflow = "scroll";

        // add innerdiv
        var inner = document.createElement("div");
        inner.style.width = "100%";
        outer.appendChild(inner);

        var widthWithScroll = inner.offsetWidth;

        // remove divs
        outer.parentNode.removeChild(outer);
        return widthNoScroll - widthWithScroll;
    }

    function hasScrollbar() { // проверка на боковой скролл
        return $(document).height() > $(window).height();
    }

    //validation
    $.validator.setDefaults({
        // debug: true,
        ignore: [],
    });
    $.validator.addMethod("checkMin", function (value, element, param) {
        var num = value.replace(/[^0-9]/g,"");
        return num.length === 11;
    // }, "Введите не меньше 11 символов");
    });

    $(".form-modal").validate({
       rules: {
           name: {
               required: true
           },
           email: {
               required: true
           },
           tel: {
               required: false,
               checkMin: true
           }
       },
        submitHandler: function(form) {
            // console.log("submit");
            $.ajax({
                method: "POST",
                url: "/orders/store",
                data: $(form).serialize()
            })
                .done(function(data) {
                    console.log( "success" );
                    $(".default-modal.active").removeClass("active");
                    var modal = $(".modal-thx");
                    var form = $(".form-modal");
                    form[0].reset();

                    setTimeout(function() {
                        modal.addClass("active");
                    }, 300);
                })
                .fail(function(data) {
                    console.log( "error" );
                })
                .always(function(data) {
                    // console.log( "complete" );
                });
        }
    });

    $(".form-consult").validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            }
        },
        submitHandler: function(form) {
            // console.log("submit");
            $.ajax({
                method: "POST",
                // url: "some.php",
                url: "/orders/store",
                data: $(form).serialize()
            })
                .done(function() {
                    console.log( "success" );
                    var form = $(".form-consult");
                    form[0].reset();

                    showThx();
                })
                .fail(function() {
                    console.log( "error" );
                })
                .always(function() {
                    // console.log( "complete" );
                });
        }
    });

    $(".form-audit").validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            }
        },
        submitHandler: function(form) {
            // console.log("submit");
            $.ajax({
                method: "POST",
                // url: "some.php",
                url: "/orders/store",
                data: $(form).serialize()
            })
                .done(function() {
                    console.log( "success" );
                    var form = $(".form-audit");
                    form[0].reset();

                    showThx();
                })
                .fail(function() {
                    console.log( "error" );
                })
                .always(function() {
                    // console.log( "complete" );
                });
        }
    });

    $(".form-service-consult").validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            },
            agree: {
                required: true
            }
        },
        submitHandler: function(form) {
            // console.log("submit");
            $.ajax({
                method: "POST",
                // url: "http://www.mocky.io/v2/5be40d0c2f00003600d9f2e2",
                // url: "http://www.mocky.io/v2/5be2fe852f00002b00ca21ff",
                url: "/orders/store",
                data: $(form).serialize()
            })
                .done(function() {
                    console.log( "success" );
                    var form = $(".form-service-consult");
                    form[0].reset();
                    showThx();
                })
                .fail(function() {
                    console.log( "error" );
                })
                .always(function() {
                    // console.log( "complete" );
                });
        }
    });
    $(".form-partner-consult").validate({
        rules: {
            name: {
                required: true
            },
            email: {
                required: true
            },
            agree: {
                required: true
            }
        },
        submitHandler: function(form) {
            // console.log("submit");
            $.ajax({
                method: "POST",
                url: "/orders/store",
                // url: "http://www.mocky.io/v2/5be40d0c2f00003600d9f2e2",
                data: $(form).serialize()
            })
                .done(function() {
                    console.log( "success" );
                    var form = $(".form-partner-consult");
                    form[0].reset();

                    showThx();
                })
                .fail(function() {
                    console.log( "error" );
                })
                .always(function() {
                    // console.log( "complete" );
                });
        }
    });

    function showThx() {
        $("body").addClass("modal-open");
        $(".default-modal.active").removeClass("active");
        var modal = $(".modal-thx");
        $('html').addClass('no-scroll');
        if (hasScrollbar()) {
            var scrollWidth = getScrollbarWidth();
            $("html").css({
                "margin-right": scrollWidth
            });
            modal.addClass("active");
            $(".header").css("padding-right", scrollWidth);
        } else {
            modal.addClass("active");
        }
    }

    $('.testimonials__item').click(function(){
        var review_id = $(this).data('review_id');
        var token = $('input[name=_token]').val();
        var lang = $('html').attr('lang');

        $.post('/'+lang+'/ajax/review/'+review_id,{'_token':token},function(data){
            var image = '';
            for(var i in data.images)
            {
                if(data.images[i].is_main==0)
                {
                    image = data.images[i].path;
                    break;
                }
            }
            $('.modal-review-main img').attr('src',image);
            $('.modal-review-main .phone').text(data.phone);
            $('.modal-review-main .email').text(data.email);
            var lang_field = 'address_'+lang;
            $('.modal-review-main .address').text(data[lang_field]);
            var title_field = 'title_'+lang;
            $('.modal-review-main .default-modal__title').text(data[title_field]);
            var text_field = 'text_'+lang;
            $('.modal-review-main .modal-review-main__text').html(data[text_field]);
            var signature_field = 'signature_'+lang;
            $('.modal-review-main .modal-review-main__who').text(data[signature_field]);
        })
    });

});